#include "mainwindow.h"
#include <QMenuBar>
#include <QMenu>
#include <QAction>
#include <QDebug>
#include <QMessageBox>
#include <QToolBar>
#include <QPushButton>
#include <QStatusBar>
#include <QLabel>
#include <QTextEdit>
#include <QDockWidget>
#include <QDialog>
#include <QFileDialog>
#include <stdio.h>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    //菜单栏
    QMenuBar *mBar = menuBar();
    //添加菜单
    QMenu *pFile = mBar->addMenu("文件");
    QMenu *pAbout = mBar->addMenu("帮助");

    //添加菜单项
    QAction *pabout = pAbout->addAction("关于");
    connect(pabout,&QAction::triggered,
            [=]()
    {
        QMessageBox::about(this,"about","this is Qt");
    }
            );
    QAction *question = pAbout->addAction("问题");
    connect(question,&QAction::triggered,
            [=]()
    {
//       QMessageBox::question(this,"问题","Do you want to save it ?");
        //修改question下对应的按钮
         int ret = QMessageBox::question(this,"问题","Do you want to save it ?",QMessageBox::Ok |QMessageBox::Cancel);
         switch (ret) {
            case QMessageBox::Ok :QMessageBox::information(this,"提示","保存成功！"); break ;
            case QMessageBox::Cancel :QMessageBox::information(this,"提示","未保存"); break;
            default: break;
         }

    }
            );
//*****************打开文件，找到对应文件，显示路径
    QAction *Openfile = pFile->addAction("文件对话框");
    connect(Openfile,&QAction::triggered,
            [=]()
    {
        QString temp = QFileDialog::getOpenFileName(this,"打开文件","../","Sources (*.cpp *.c *.h);;Text files (*.txt);;all files (*.*)");
        qDebug()<<temp;

    }
            );

    QAction *pNew=pFile->addAction("新建");
    connect(pNew,&QAction::triggered,
            [=]()
            {
            //qDebug()<<"新建被按下";
            qDebug("Hello World!");
            QMessageBox::information(this,"提示","新建被按下");

//            QMessageBox msgBox;
//            msgBox.setText("The document has been modified.");
//            msgBox.setInformativeText("Do you want to save your changes?");
//            msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
//            msgBox.setDefaultButton(QMessageBox::Save);
//            int ret = msgBox.exec();

            }
            );
    pFile->addSeparator();//添加分割线
    QAction *pOpen =pFile->addAction("打开");

//****************模态对话框
    pFile->addSeparator();//添加分割线
    QAction *pMotai =pFile->addAction("模态对话框");
    connect(pMotai,&QAction::triggered,
            [=]()
    {
        QDialog dlg;
        dlg.exec();
        qDebug("close !");
    }

            );
//****************非模态对话框
    pFile->addSeparator();//添加分割线
    QAction *pNmotai =pFile->addAction("非模态对话框");
    connect(pNmotai,&QAction::triggered,
            [=]()
    {
//*******************这两种方法可以使用，但是会造成内存堆积，直到程序整体结束时才会释放
//        第一种将对象定义在头文件中
// 1       dlg.show();
//        qDebug("show !");

// 2       QDialog *dlg = new QDialog(this);
//        dlg->show();
//        qDebug("show !");

// 3
        QDialog *dlg = new QDialog;
        dlg->setAttribute(Qt::WA_DeleteOnClose); //该对话框一关闭内存就会被释放
        dlg->show();
        qDebug("show !");
    }

            );

    //工具栏 菜单项的快捷方式
    QToolBar *toolBar = addToolBar("Bar");
    //工具栏添加快捷键
    toolBar->addAction(pNew);
    QPushButton *b = new QPushButton(this);
    b->setText("@_@");

    toolBar->addWidget(b);
    connect(b,&QPushButton::clicked,
            [=]()
    {
        b->setText("123");
    }
            );

    //状态栏
    QStatusBar *sBar = statusBar();
    QLabel *lable = new QLabel(this);
    lable->setText("Normal text file");
    sBar->addWidget(lable);

    sBar->addWidget(new QLabel("2",this));//从左往右添加
    sBar->addPermanentWidget(new QLabel("3",this)); //从右往左添加

    //核心控件
    QTextEdit *textEdit = new QTextEdit();  //文本编辑
    setCentralWidget(textEdit);
    //浮动窗口
    QDockWidget *dockWidget = new QDockWidget();
    addDockWidget(Qt::LeftDockWidgetArea,dockWidget);//将浮动窗口默认放在左边
}

MainWindow::~MainWindow()
{
}

