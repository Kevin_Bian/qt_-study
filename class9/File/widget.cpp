#include "widget.h"
#include "ui_widget.h"
#include <QFile>
#include <QFileDialog>

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);

}

Widget::~Widget()
{
    delete ui;
}


void Widget::on_ReadButton_clicked()
{
    QString path = QFileDialog::getOpenFileName(this,"打开文件","../","TXT(*.txt);;all(*.*)");

    if(!path.isEmpty())
    {
        //文件对象
        QFile file(path);
        //打开文件
        bool isok = file.open(QIODevice::ReadOnly);
        if(isok)
        {
            //读文件,只能识别utf8格式
            QByteArray array = file.readAll();
            //显示到编辑区
            ui->textEdit->setText(array);
        }
        file.close();
     }
}

void Widget::on_pushButton_2_clicked()
{
}
