#include "widget.h"

#include <QApplication> //头文件和类名一样

int main(int argc, char *argv[])
{
    QApplication a(argc, argv); //有且只有一个应用程序类的对象
    Widget w;   //Widget继承于QWidget，QWidget是一个窗口基类，所以Widget也是一个窗口类（派生类，子类），w是定义的一个对象，就是一个窗口
    w.setWindowTitle("BWK");   //设置界面标题
    w.show();   //窗口创建出来默认是隐藏的，需要人为将其显示出来
    return a.exec();    //让程序一直执行，等待用户操作，即等待事件的发生。
}
