#include "widget.h"
#include "ui_widget.h"
#include "ctrl.h"
#include "QString"


Widget::Widget(QWidget *parent)  //构造函数定义
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);         //自动关联注册的信号和槽
    connect(ui->signIn,SIGNAL(clicked()),this,SLOT(signIn_clicked_slots()));      //手动关联登录的信号和槽
}

Widget::~Widget()      //析构函数定义
{
    delete ui;
}


void Widget::on_signUp_clicked()    //注册（自动）
{
    //qDebug("Hello World!");
}
void Widget::signIn_clicked_slots()    //自定义槽函数：登录（手动）
{
    //qDebug("Good Bye!");
    QString username = ui->userEdit->text();    //获取userEdit中的数据并将其赋给username
    QString passward = ui->passwardEdit->text();
    if(username=="15129774793" && passward=="12345")
    {
        Ctrl *ct = new Ctrl;  //在堆里面定义对象，指针变量
        ct->setWindowTitle("Control Box");  //设置界面标题
        ct->setGeometry(this->geometry());
        ct->show();
     }
}
