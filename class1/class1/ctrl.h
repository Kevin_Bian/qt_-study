#ifndef CTRL_H
#define CTRL_H

#include <QWidget>

namespace Ui {
class Ctrl;
}

class Ctrl : public QWidget
{
    Q_OBJECT

public:
    explicit Ctrl(QWidget *parent = nullptr);
    ~Ctrl();

private slots:
    void on_backButton_clicked();

private:
    Ui::Ctrl *ui;
};

#endif // CTRL_H
