#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget       //Widge类继承于基类QWidge
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);  //构造函数
    ~Widget();  //析构函数

private slots:

    void on_signUp_clicked();       //自动关联注册的信号和槽
    void signIn_clicked_slots();       //手动关联登录的信号和槽
private:
    Ui::Widget *ui;
};
#endif // WIDGET_H
