#include "ctrl.h"
#include "ui_ctrl.h"

Ctrl::Ctrl(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Ctrl)
{
    ui->setupUi(this);
}

Ctrl::~Ctrl()
{
    delete ui;
}

void Ctrl::on_backButton_clicked()
{
    this->close();
}
