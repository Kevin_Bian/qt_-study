/********************************************************************************
** Form generated from reading UI file 'ctrl.ui'
**
** Created by: Qt User Interface Compiler version 5.12.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CTRL_H
#define UI_CTRL_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Ctrl
{
public:
    QGridLayout *gridLayout;
    QSpacerItem *verticalSpacer;
    QSpacerItem *horizontalSpacer_3;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout;
    QPushButton *LEDButton;
    QLabel *LED;
    QSpacerItem *horizontalSpacer;
    QVBoxLayout *verticalLayout_2;
    QPushButton *settingButton;
    QLabel *setting;
    QSpacerItem *horizontalSpacer_2;
    QVBoxLayout *verticalLayout_3;
    QPushButton *backButton;
    QLabel *back;
    QSpacerItem *horizontalSpacer_4;
    QSpacerItem *verticalSpacer_2;

    void setupUi(QWidget *Ctrl)
    {
        if (Ctrl->objectName().isEmpty())
            Ctrl->setObjectName(QString::fromUtf8("Ctrl"));
        Ctrl->resize(800, 600);
        Ctrl->setMinimumSize(QSize(80, 80));
        gridLayout = new QGridLayout(Ctrl);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        verticalSpacer = new QSpacerItem(20, 209, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer, 0, 1, 1, 1);

        horizontalSpacer_3 = new QSpacerItem(178, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_3, 1, 0, 1, 1);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        LEDButton = new QPushButton(Ctrl);
        LEDButton->setObjectName(QString::fromUtf8("LEDButton"));
        LEDButton->setMinimumSize(QSize(80, 80));
        LEDButton->setStyleSheet(QString::fromUtf8("border-image: url(:/pic/led.png);"));

        verticalLayout->addWidget(LEDButton);

        LED = new QLabel(Ctrl);
        LED->setObjectName(QString::fromUtf8("LED"));
        QFont font;
        font.setPointSize(12);
        LED->setFont(font);
        LED->setLayoutDirection(Qt::LeftToRight);
        LED->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(LED);


        horizontalLayout->addLayout(verticalLayout);

        horizontalSpacer = new QSpacerItem(68, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        settingButton = new QPushButton(Ctrl);
        settingButton->setObjectName(QString::fromUtf8("settingButton"));
        settingButton->setMinimumSize(QSize(80, 80));
        settingButton->setStyleSheet(QString::fromUtf8("border-image: url(:/pic/set.png);"));

        verticalLayout_2->addWidget(settingButton);

        setting = new QLabel(Ctrl);
        setting->setObjectName(QString::fromUtf8("setting"));
        setting->setFont(font);
        setting->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(setting);


        horizontalLayout->addLayout(verticalLayout_2);

        horizontalSpacer_2 = new QSpacerItem(58, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        backButton = new QPushButton(Ctrl);
        backButton->setObjectName(QString::fromUtf8("backButton"));
        backButton->setMinimumSize(QSize(80, 80));
        backButton->setStyleSheet(QString::fromUtf8("border-image: url(:/pic/back.png);"));

        verticalLayout_3->addWidget(backButton);

        back = new QLabel(Ctrl);
        back->setObjectName(QString::fromUtf8("back"));
        back->setFont(font);
        back->setAlignment(Qt::AlignCenter);

        verticalLayout_3->addWidget(back);


        horizontalLayout->addLayout(verticalLayout_3);


        gridLayout->addLayout(horizontalLayout, 1, 1, 1, 1);

        horizontalSpacer_4 = new QSpacerItem(178, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_4, 1, 2, 1, 1);

        verticalSpacer_2 = new QSpacerItem(20, 238, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer_2, 2, 1, 1, 1);


        retranslateUi(Ctrl);

        QMetaObject::connectSlotsByName(Ctrl);
    } // setupUi

    void retranslateUi(QWidget *Ctrl)
    {
        Ctrl->setWindowTitle(QApplication::translate("Ctrl", "Form", nullptr));
        LEDButton->setText(QString());
        LED->setText(QApplication::translate("Ctrl", "LED\347\201\257", nullptr));
        settingButton->setText(QString());
        setting->setText(QApplication::translate("Ctrl", "\350\256\276\347\275\256", nullptr));
        backButton->setText(QString());
        back->setText(QApplication::translate("Ctrl", "\351\200\200\345\207\272", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Ctrl: public Ui_Ctrl {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CTRL_H
