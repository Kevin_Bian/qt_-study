#include "widget.h"
#include "ui_widget.h"
#include "QMessageBox"
#include <QSerialPortInfo>

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
    serialPort = new QSerialPort(this);
    connect(serialPort,SIGNAL(readyRead()),this,SLOT(serialPortReadyRead_Slot())); //手动
    QStringList serialNamePort;
    foreach (const QSerialPortInfo & info,QSerialPortInfo::availablePorts()) {
        serialNamePort<<info.portName();
    }
    ui->serialBox->addItems(serialNamePort);
}

Widget::~Widget()
{
    delete ui;
}
void Widget::serialPortReadyRead_Slot()
{
    QString SBUF;
    SBUF = QString(serialPort->readAll());
    ui->receiveEdit->appendPlainText(SBUF);
}

void Widget::on_openBt_clicked()
{
    QSerialPort::BaudRate baudRate;  //bps
    QSerialPort::DataBits dataBits;
    QSerialPort::StopBits stopBits;
    QSerialPort::Parity checkBits;

    if(ui->bpsBox->currentText() == "1200"){
        baudRate = QSerialPort::Baud1200;
    }else if(ui->bpsBox->currentText() == "2400"){
        baudRate = QSerialPort::Baud2400;
    }else if(ui->bpsBox->currentText() == "4800"){
        baudRate = QSerialPort::Baud4800;
    }else if(ui->bpsBox->currentText() == "9600"){
        baudRate = QSerialPort::Baud9600;
    }else if(ui->bpsBox->currentText() == "115200"){
        baudRate = QSerialPort::Baud115200;
    }

    if(ui->dataBox->currentText() == "5"){
        dataBits = QSerialPort::Data5;
    }else if(ui->dataBox->currentText() == "6"){
        dataBits = QSerialPort::Data6;
    }else if(ui->dataBox->currentText() == "7"){
        dataBits = QSerialPort::Data7;
    }else if(ui->dataBox->currentText() == "8"){
        dataBits = QSerialPort::Data8;
    }

    if(ui->stopBox->currentText() == "1"){
        stopBits = QSerialPort::OneStop;
    }else  if(ui->stopBox->currentText() == "1.5"){
        stopBits = QSerialPort::OneAndHalfStop;
    }else  if(ui->stopBox->currentText() == "2"){
        stopBits = QSerialPort::TwoStop;
    }

    if(ui->checkBox->currentText() == "none"){
        checkBits = QSerialPort::NoParity;
    }

    serialPort->setPortName(ui->serialBox->currentText());
    serialPort->setBaudRate(baudRate);
    serialPort->setDataBits(dataBits);
    serialPort->setStopBits(stopBits);
    serialPort->setParity(checkBits);

   if(serialPort->open(QIODevice::ReadWrite)==true){
        QMessageBox::information(this,"提示","串口打开成功");
    }
    else{
        QMessageBox::critical(this,"提示","串口打开失败");
    }
}

void Widget::on_closeBt_clicked()
{
    serialPort->close();
}

void Widget::on_sendBt_clicked()
{
    serialPort->write(ui->sendEdit->text().toLocal8Bit().data());
}

void Widget::on_cleanBt_clicked()
{
    ui->receiveEdit->clear();
    ui->sendEdit->clear();
}
