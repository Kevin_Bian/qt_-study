/********************************************************************************
** Form generated from reading UI file 'widget.ui'
**
** Created by: Qt User Interface Compiler version 5.12.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WIDGET_H
#define UI_WIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Widget
{
public:
    QGridLayout *gridLayout_2;
    QPlainTextEdit *receiveEdit;
    QHBoxLayout *horizontalLayout;
    QGridLayout *gridLayout;
    QLabel *label;
    QComboBox *serialBox;
    QLabel *label_2;
    QComboBox *bpsBox;
    QLabel *label_3;
    QComboBox *dataBox;
    QLabel *label_4;
    QComboBox *stopBox;
    QLabel *label_5;
    QComboBox *checkBox;
    QVBoxLayout *verticalLayout_2;
    QGroupBox *groupBox;
    QLabel *label_6;
    QLineEdit *sendEdit;
    QHBoxLayout *horizontalLayout_6;
    QPushButton *openBt;
    QSpacerItem *horizontalSpacer;
    QPushButton *closeBt;
    QSpacerItem *horizontalSpacer_2;
    QPushButton *sendBt;
    QSpacerItem *horizontalSpacer_3;
    QPushButton *cleanBt;

    void setupUi(QWidget *Widget)
    {
        if (Widget->objectName().isEmpty())
            Widget->setObjectName(QString::fromUtf8("Widget"));
        Widget->resize(796, 604);
        QFont font;
        font.setBold(true);
        font.setWeight(75);
        Widget->setFont(font);
        gridLayout_2 = new QGridLayout(Widget);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        receiveEdit = new QPlainTextEdit(Widget);
        receiveEdit->setObjectName(QString::fromUtf8("receiveEdit"));
        receiveEdit->setReadOnly(true);

        gridLayout_2->addWidget(receiveEdit, 0, 0, 1, 1);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        label = new QLabel(Widget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setFont(font);

        gridLayout->addWidget(label, 0, 0, 1, 1);

        serialBox = new QComboBox(Widget);
        serialBox->addItem(QString());
        serialBox->setObjectName(QString::fromUtf8("serialBox"));

        gridLayout->addWidget(serialBox, 0, 1, 1, 1);

        label_2 = new QLabel(Widget);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setFont(font);

        gridLayout->addWidget(label_2, 1, 0, 1, 1);

        bpsBox = new QComboBox(Widget);
        bpsBox->addItem(QString());
        bpsBox->addItem(QString());
        bpsBox->addItem(QString());
        bpsBox->addItem(QString());
        bpsBox->addItem(QString());
        bpsBox->setObjectName(QString::fromUtf8("bpsBox"));

        gridLayout->addWidget(bpsBox, 1, 1, 1, 1);

        label_3 = new QLabel(Widget);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setFont(font);

        gridLayout->addWidget(label_3, 2, 0, 1, 1);

        dataBox = new QComboBox(Widget);
        dataBox->addItem(QString());
        dataBox->addItem(QString());
        dataBox->addItem(QString());
        dataBox->addItem(QString());
        dataBox->setObjectName(QString::fromUtf8("dataBox"));

        gridLayout->addWidget(dataBox, 2, 1, 1, 1);

        label_4 = new QLabel(Widget);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setFont(font);

        gridLayout->addWidget(label_4, 3, 0, 1, 1);

        stopBox = new QComboBox(Widget);
        stopBox->addItem(QString());
        stopBox->addItem(QString());
        stopBox->addItem(QString());
        stopBox->setObjectName(QString::fromUtf8("stopBox"));

        gridLayout->addWidget(stopBox, 3, 1, 1, 1);

        label_5 = new QLabel(Widget);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setFont(font);

        gridLayout->addWidget(label_5, 4, 0, 1, 1);

        checkBox = new QComboBox(Widget);
        checkBox->addItem(QString());
        checkBox->setObjectName(QString::fromUtf8("checkBox"));

        gridLayout->addWidget(checkBox, 4, 1, 1, 1);


        horizontalLayout->addLayout(gridLayout);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        groupBox = new QGroupBox(Widget);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setMinimumSize(QSize(150, 50));
        label_6 = new QLabel(groupBox);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setGeometry(QRect(80, 20, 291, 31));
        QFont font1;
        font1.setPointSize(15);
        font1.setBold(false);
        font1.setWeight(50);
        label_6->setFont(font1);
        label_6->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(groupBox);

        sendEdit = new QLineEdit(Widget);
        sendEdit->setObjectName(QString::fromUtf8("sendEdit"));

        verticalLayout_2->addWidget(sendEdit);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        openBt = new QPushButton(Widget);
        openBt->setObjectName(QString::fromUtf8("openBt"));

        horizontalLayout_6->addWidget(openBt);

        horizontalSpacer = new QSpacerItem(13, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer);

        closeBt = new QPushButton(Widget);
        closeBt->setObjectName(QString::fromUtf8("closeBt"));

        horizontalLayout_6->addWidget(closeBt);

        horizontalSpacer_2 = new QSpacerItem(13, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_2);

        sendBt = new QPushButton(Widget);
        sendBt->setObjectName(QString::fromUtf8("sendBt"));

        horizontalLayout_6->addWidget(sendBt);

        horizontalSpacer_3 = new QSpacerItem(13, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_3);

        cleanBt = new QPushButton(Widget);
        cleanBt->setObjectName(QString::fromUtf8("cleanBt"));

        horizontalLayout_6->addWidget(cleanBt);


        verticalLayout_2->addLayout(horizontalLayout_6);


        horizontalLayout->addLayout(verticalLayout_2);


        gridLayout_2->addLayout(horizontalLayout, 1, 0, 1, 1);


        retranslateUi(Widget);

        bpsBox->setCurrentIndex(3);
        dataBox->setCurrentIndex(3);


        QMetaObject::connectSlotsByName(Widget);
    } // setupUi

    void retranslateUi(QWidget *Widget)
    {
        Widget->setWindowTitle(QApplication::translate("Widget", "Widget", nullptr));
        label->setText(QApplication::translate("Widget", "\344\270\262\345\217\243\345\217\267 \357\274\232", nullptr));
        serialBox->setItemText(0, QString());

        label_2->setText(QApplication::translate("Widget", "\346\263\242\347\211\271\347\216\207 \357\274\232", nullptr));
        bpsBox->setItemText(0, QApplication::translate("Widget", "1200", nullptr));
        bpsBox->setItemText(1, QApplication::translate("Widget", "2400", nullptr));
        bpsBox->setItemText(2, QApplication::translate("Widget", "4800", nullptr));
        bpsBox->setItemText(3, QApplication::translate("Widget", "9600", nullptr));
        bpsBox->setItemText(4, QApplication::translate("Widget", "115200", nullptr));

        label_3->setText(QApplication::translate("Widget", "\346\225\260\346\215\256\344\275\215 \357\274\232", nullptr));
        dataBox->setItemText(0, QApplication::translate("Widget", "5", nullptr));
        dataBox->setItemText(1, QApplication::translate("Widget", "6", nullptr));
        dataBox->setItemText(2, QApplication::translate("Widget", "7", nullptr));
        dataBox->setItemText(3, QApplication::translate("Widget", "8", nullptr));

        label_4->setText(QApplication::translate("Widget", "\345\201\234\346\255\242\344\275\215 \357\274\232", nullptr));
        stopBox->setItemText(0, QApplication::translate("Widget", "1", nullptr));
        stopBox->setItemText(1, QApplication::translate("Widget", "1.5", nullptr));
        stopBox->setItemText(2, QApplication::translate("Widget", "2", nullptr));

        label_5->setText(QApplication::translate("Widget", "\346\240\241\351\252\214\344\275\215 \357\274\232", nullptr));
        checkBox->setItemText(0, QApplication::translate("Widget", "none", nullptr));

        groupBox->setTitle(QString());
        label_6->setText(QApplication::translate("Widget", "\350\207\252\345\210\266\344\270\262\345\217\243\345\212\251\346\211\213", nullptr));
        openBt->setText(QApplication::translate("Widget", "\346\211\223\345\274\200", nullptr));
        closeBt->setText(QApplication::translate("Widget", "\345\205\263\351\227\255", nullptr));
        sendBt->setText(QApplication::translate("Widget", "\345\217\221\351\200\201", nullptr));
        cleanBt->setText(QApplication::translate("Widget", "\346\270\205\347\251\272", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Widget: public Ui_Widget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WIDGET_H
