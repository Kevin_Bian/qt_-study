#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QTcpServer>   //监听
#include <QTcpSocket>   //通信
#include <QString>

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();

    QTcpServer *tcpServer;  //监听
    QTcpSocket *tcpSocket;  //通信


private slots:
    void on_openBt_clicked();

    void on_closeBt_clicked();

    void on_sendBT_clicked();

    void newConnection_Slot();
    void readyRead_Slot();


private:
    Ui::Widget *ui;
};
#endif // WIDGET_H
