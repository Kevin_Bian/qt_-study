﻿#ifndef VIP_H
#define VIP_H

#include <QDialog>

namespace Ui {
class VIP;
}

class VIP : public QDialog
{
    Q_OBJECT

public:
    explicit VIP(QWidget *parent = 0);
    ~VIP();
public slots:
    void add();
    void alter();
    void DEL();
private:
    Ui::VIP *ui;
};

#endif // VIP_H
