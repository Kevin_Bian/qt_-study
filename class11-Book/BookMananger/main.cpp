﻿#include "mainwindow.h"
#include <QApplication>
#include "login.h"
#include <QMessageBox>
#include <QDebug>
#pragma execution_character_set("utf-8");
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;

    Login l;
    l.setWindowTitle("欢迎登录界面");//设置窗体标题
    if (l.exec() == QDialog::Accepted)//调用login.exec()，阻塞主控制流，直到完成返回，继续执行主控制流
    {
       w.show();
       return a.exec();

    }
    return a.exec();
}
