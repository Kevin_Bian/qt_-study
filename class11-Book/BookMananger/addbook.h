﻿#ifndef ADDBOOK_H
#define ADDBOOK_H

#include <QDialog>
#include<QSqlQuery>
namespace Ui {
class addBook;
}

class addBook : public QDialog
{
    Q_OBJECT

public:
    explicit addBook(QWidget *parent = 0);
    ~addBook();

public slots:
    void insert();
    void alter();
    void deleteBook();
private:
    Ui::addBook *ui;
};

#endif // ADDBOOK_H
