﻿#include "vip.h"
#include "ui_vip.h"
#include <QMessageBox>
#include <QSql>
#include <QDialog>
#include <QDebug>
#include <QMessageBox>
#include <QSqlError>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlQueryModel>
#include <QRadioButton>
#include <QPushButton>
QSqlDatabase db2 = QSqlDatabase::addDatabase("QODBC");
#pragma execution_character_set("utf-8");

VIP::VIP(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::VIP)
{
    qDebug()<<"ODBC driver?"<<db2.isValid();//数据库驱动类型为SQL Server
    QString dsn = QString::fromLocal8Bit("BookStore");      //数据源名称
    db2.setHostName("localhost");                        //选择本地主机，127.0.1.1
    db2.setDatabaseName(dsn);                            //设置数据源名称
    db2.setUserName("sa");                               //登录用户
    db2.setPassword("BB.WW.KK");
    db2.open();//打开数据库

    ui->setupUi(this);
    setWindowTitle("会员系统");
    ui->LE0->setPlaceholderText(tr("请输入会员姓名!"));
    ui->LE1->setPlaceholderText("请输入会员电话!");
    connect(ui->ADD,&QPushButton::clicked,this,&VIP::add);
    connect(ui->UPD,&QPushButton::clicked,this,&VIP::alter);
    connect(ui->DEL,&QPushButton::clicked,this,&VIP::DEL);
    connect(ui->EXIT,&QPushButton::clicked,this,&VIP::close);
}

void VIP::add()
{
    QSqlQuery query(db2);
      query.prepare("INSERT INTO VIP (name,phoneNum) values (:name,:phoneNum)");
      query.bindValue(0,ui->LE0->text());
      query.bindValue(1,ui->LE1->text());
      if(query.exec()==QDialog::Accepted)
      {
          QMessageBox::information(this,tr("info"),tr("插入成功"));
          //清除数据
          ui->LE0->clear();
          ui->LE1->clear();
      }
      else
      {
          QMessageBox::warning(this, tr("警告！"),tr("插入错误！"),QMessageBox::Yes);
          ui->LE0->clear();
          ui->LE1->clear();
          ui->LE0->setFocus();
      }
}

void VIP::DEL(){
    QSqlQuery query3(db2);
    if(!ui->LE0->text().isEmpty())
    {
        query3.prepare("delete from VIP where name = :name");
        query3.bindValue(0,ui->LE0->text());
    }
    else if(!ui->LE1->text().isEmpty())
    {
        query3.prepare("delete from VIP where phoneNum = :phoneNum");
        query3.bindValue(0,ui->LE1->text());
    }
    if(query3.exec()==QDialog::Accepted)
    {
        QMessageBox::information(this,tr("info"),tr("删除成功"));
        //清除数据
        ui->LE0->clear();
        ui->LE1->clear();
    }
    else
    {
        QMessageBox::warning(this, tr("警告！"),tr("删除失败！"),QMessageBox::Yes);
        ui->LE0->clear();
        ui->LE1->clear();
    }
}
void VIP::alter(){
    QSqlQuery query(db2);
    if(ui->CB1->isChecked())
    {
       if(!ui->LE1->text().isEmpty())
       {
       query.prepare("update VIP set phoneNum = :phoneNum where name = :name");
       query.bindValue(":phoneNum",ui->LE1->text());
       }
       query.bindValue(":name",ui->LE0->text());
       if(query.exec()==QDialog::Accepted)
       {
           QMessageBox::information(this,tr("info"),tr("修改成功"));
           //清除数据
           ui->LE0->clear();
           ui->LE1->clear();
       }
       else
       {
           QMessageBox::warning(this, tr("警告！"),tr("修改错误！"),QMessageBox::Yes);
           ui->LE0->clear();
           ui->LE1->clear();
       }
    }
    if(ui->CB2->isChecked())
    {
        if(!ui->LE0->text().isEmpty())
        {
        query.prepare("update VIP set name = :name where phoneNum = :phoneNum");
        query.bindValue(":name",ui->LE0->text());
        }
        query.bindValue(":phoneNum",ui->LE1->text());
        if(query.exec()==QDialog::Accepted)
        {
            QMessageBox::information(this,tr("info"),tr("修改成功"));
            //清除数据
            ui->LE0->clear();
            ui->LE1->clear();
        }
        else
        {
            QMessageBox::warning(this, tr("警告！"),tr("修改错误！"),QMessageBox::Yes);
            ui->LE0->clear();
            ui->LE1->clear();
        }
    }
}
VIP::~VIP()
{
    delete ui;
}
