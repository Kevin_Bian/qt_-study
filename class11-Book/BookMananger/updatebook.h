#ifndef UPDATEBOOK_H
#define UPDATEBOOK_H

#include <QDialog>

namespace Ui {
class updateBook;
}

class updateBook : public QDialog
{
    Q_OBJECT

public:
    explicit updateBook(QWidget *parent = 0);
    ~updateBook();

private:
    Ui::updateBook *ui;
};

#endif // UPDATEBOOK_H
