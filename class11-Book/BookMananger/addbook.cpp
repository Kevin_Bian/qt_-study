﻿#include "addbook.h"
#include "ui_addbook.h"
#include <QMessageBox>
#include <QSql>
#include <QDialog>
#include <QDebug>
#include <QMessageBox>
#include <QSqlError>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlQueryModel>
#include <QRadioButton>
#include <QPushButton>
QSqlDatabase db = QSqlDatabase::addDatabase("QODBC");
#pragma execution_character_set("utf-8");
addBook::addBook(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::addBook)
{
    qDebug()<<"ODBC driver?"<<db.isValid();//数据库驱动类型为SQL Server
    QString dsn = QString::fromLocal8Bit("BookStore");      //数据源名称
    db.setHostName("localhost");                        //选择本地主机，127.0.1.1
    db.setDatabaseName(dsn);                            //设置数据源名称
    db.setUserName("sa");                               //登录用户
    db.setPassword("BB.WW.KK");
    db.open();//打开数据库
    ui->setupUi(this);
    setWindowTitle("存入库存");
    connect(ui->addBTN1,&QPushButton::clicked,this,&addBook::insert);
    connect(ui->AlterBTN1,&QPushButton::clicked,this,&addBook::alter);
    connect(ui->delBTN1,&QPushButton::clicked,this,&addBook::deleteBook);
    connect(ui->exitBTN1,&QPushButton::clicked,this,&QWidget::close);
}

addBook::~addBook()
{
    delete ui;
}

void addBook::deleteBook()
{
    QSqlQuery query3(db);
    if(!ui->LE0->text().isEmpty())
    {
        query3.prepare("delete from book where name = :name");
        query3.bindValue(":name",ui->LE0->text());
    }
    else if(!ui->LE3->text().isEmpty())
    {
        query3.prepare("delete from book where ISBN = :ISBN");
        query3.bindValue(":ISBN",ui->LE3->text());
    }
    if(query3.exec()==QDialog::Accepted)
    {
        QMessageBox::information(this,tr("info"),tr("删除成功"));
        //清除数据
        ui->LE0->clear();
        ui->LE1->clear();
        ui->LE2->clear();
        ui->LE3->clear();
        ui->LE4->clear();
        ui->LE5->clear();
        ui->LE6->clear();
        ui->LE7->clear();
        ui->LE8->clear();
        ui->LE9->clear();
        ui->LE10->clear();

    }
    else
    {
        QMessageBox::warning(this, tr("警告！"),tr("删除失败！"),QMessageBox::Yes);
        ui->LE0->clear();
        ui->LE1->clear();
        ui->LE2->clear();
        ui->LE3->clear();
        ui->LE4->clear();
        ui->LE5->clear();
        ui->LE6->clear();
        ui->LE7->clear();
        ui->LE8->clear();
        ui->LE9->clear();
        ui->LE10->clear();
    }
}
void addBook::alter()
{
    QSqlQuery query(db);
    QSqlQuery query2(db);
    if(ui->CB1->isChecked())
    {
       if(!ui->LE1->text().isEmpty())
       {
       query.prepare("update book set author = :author where name = :name");
       query.bindValue(":author",ui->LE1->text());
       }
       if(!ui->LE2->text().isEmpty())
       {
       query.prepare("update book set price = :price where name = :name");
       query.bindValue(":price",ui->LE2->text());
       }
       if(!ui->LE3->text().isEmpty())
       {
       query.prepare("update book set ISBN = :ISBN where name = :name");
       query.bindValue(":ISBN",ui->LE3->text());
       }
       if(!ui->LE4->text().isEmpty())
       {
       query.prepare("update book set press = :press where name = :name");
       query.bindValue(":press",ui->LE4->text());
       }
       if(!ui->LE5->text().isEmpty())
       {
       query.prepare("update book set language = :language where name = :name");
       query.bindValue(":language",ui->LE5->text());
       }
       if(!ui->LE6->text().isEmpty())
       {
       query.prepare("update book set page = :page where name = :name");
       query.bindValue(":page",ui->LE6->text());
       }
       if(!ui->LE7->text().isEmpty())
       {
       query.prepare("update book set package = :package where name = :name");
       query.bindValue(":package",ui->LE7->text());
       }
       if(!ui->LE8->text().isEmpty())
       {
       query.prepare("update book set publishTime = :publishTime where name = :name");
       query.bindValue(":publishTime",ui->LE8->text());
       }
       if(!ui->LE9->text().isEmpty())
       {
       query.prepare("update book set edition = :edition where name = :name");
       query.bindValue(":edition",ui->LE9->text());
       }
       if(!ui->LE10->text().isEmpty())
       {
       query.prepare("update book set num = :num where name = :name");
       query.bindValue(":num",ui->LE10->text());
       }
       query.bindValue(":name",ui->LE0->text());
       if(query.exec()==QDialog::Accepted)
       {
           QMessageBox::information(this,tr("info"),tr("修改成功"));
           //清除数据
           ui->LE0->clear();
           ui->LE1->clear();
           ui->LE2->clear();
           ui->LE3->clear();
           ui->LE4->clear();
           ui->LE5->clear();
           ui->LE6->clear();
           ui->LE7->clear();
           ui->LE8->clear();
           ui->LE9->clear();
           ui->LE10->clear();

       }
       else
       {
           QMessageBox::warning(this, tr("警告！"),tr("修改错误！"),QMessageBox::Yes);
           ui->LE0->clear();
           ui->LE1->clear();
           ui->LE2->clear();
           ui->LE3->clear();
           ui->LE4->clear();
           ui->LE5->clear();
           ui->LE6->clear();
           ui->LE7->clear();
           ui->LE8->clear();
           ui->LE9->clear();
           ui->LE10->clear();
       }
    }
    if(ui->CB2->isChecked())
    {
        if(!ui->LE0->text().isEmpty())
        {
        query2.prepare("update book set name = :name where ISBN = :ISBN");
        query2.bindValue(":name",ui->LE0->text());
        }
        if(!ui->LE1->text().isEmpty())
        {
        query2.prepare("update book set author = :author where ISBN = :ISBN");
        query2.bindValue(":author",ui->LE1->text());
        }
        if(!ui->LE2->text().isEmpty())
        {
        query2.prepare("update book set price = :price where ISBN = :ISBN");
        query2.bindValue(":price",ui->LE2->text());
        }
        if(!ui->LE4->text().isEmpty())
        {
        query2.prepare("update book set press = :press where ISBN = :ISBN");
        query2.bindValue(":press",ui->LE4->text());
        }
        if(!ui->LE5->text().isEmpty())
        {
        query2.prepare("update book set language = :language where ISBN = :ISBN");
        query2.bindValue(":language",ui->LE5->text());
        }
        if(!ui->LE6->text().isEmpty())
        {
        query2.prepare("update book set page = :page where ISBN = :ISBN");
        query2.bindValue(":page",ui->LE6->text());
        }
        if(!ui->LE7->text().isEmpty())
        {
        query2.prepare("update book set package = :package where ISBN = :ISBN");
        query2.bindValue(":package",ui->LE7->text());
        }
        if(!ui->LE8->text().isEmpty())
        {
        query2.prepare("update book set publishTime = :publishTime where ISBN = :ISBN");
        query2.bindValue(":publishTime",ui->LE8->text());
        }
        if(!ui->LE9->text().isEmpty())
        {
        query2.prepare("update book set edition = :edition where ISBN = :ISBN");
        query2.bindValue(":edition",ui->LE9->text());
        }
        if(!ui->LE10->text().isEmpty())
        {
        query2.prepare("update book set num = :num where ISBN = :ISBN");
        query2.bindValue(":num",ui->LE10->text());
        }
        query2.bindValue(":ISBN",ui->LE3->text());
        if(query2.exec()==QDialog::Accepted)
        {
            QMessageBox::information(this,tr("info"),tr("修改成功"));
            //清除数据
            ui->LE0->clear();
            ui->LE1->clear();
            ui->LE2->clear();
            ui->LE3->clear();
            ui->LE4->clear();
            ui->LE5->clear();
            ui->LE6->clear();
            ui->LE7->clear();
            ui->LE8->clear();
            ui->LE9->clear();
            ui->LE10->clear();
        }
        else
        {
            QMessageBox::warning(this, tr("警告！"),tr("修改错误！"),QMessageBox::Yes);
            ui->LE0->clear();
            ui->LE1->clear();
            ui->LE2->clear();
            ui->LE3->clear();
            ui->LE4->clear();
            ui->LE5->clear();
            ui->LE6->clear();
            ui->LE7->clear();
            ui->LE8->clear();
            ui->LE9->clear();
            ui->LE10->clear();
        }
    }
}
void addBook::insert()
{
    QSqlQuery query(db);
      query.prepare("INSERT INTO book (name,author,price,ISBN,press,language,page,package,publishTime,edition,num) values (:name,:author,:price,:ISBN,:press,:language,:page,:package,:publishTime,:edition,:num)");
      query.bindValue(0,ui->LE0->text());
      query.bindValue(1,ui->LE1->text());
      query.bindValue(2,ui->LE2->text());
      query.bindValue(3,ui->LE3->text());
      query.bindValue(4,ui->LE4->text());
      query.bindValue(5,ui->LE5->text());
      query.bindValue(6,ui->LE6->text());
      query.bindValue(7,ui->LE7->text());
      query.bindValue(8,ui->LE8->text());
      query.bindValue(9,ui->LE9->text());
      query.bindValue(10,ui->LE10->text());
      if(query.exec()==QDialog::Accepted)
      {
          QMessageBox::information(this,tr("info"),tr("插入成功"));
          //清除数据
          ui->LE0->clear();
          ui->LE1->clear();
          ui->LE2->clear();
          ui->LE3->clear();
          ui->LE4->clear();
          ui->LE5->clear();
          ui->LE6->clear();
          ui->LE7->clear();
          ui->LE8->clear();
          ui->LE9->clear();
          ui->LE10->clear();
      }
      else
      {
          QMessageBox::warning(this, tr("警告！"),tr("插入错误！"),QMessageBox::Yes);
          ui->LE0->clear();
          ui->LE1->clear();
          ui->LE2->clear();
          ui->LE3->clear();
          ui->LE4->clear();
          ui->LE5->clear();
          ui->LE6->clear();
          ui->LE7->clear();
          ui->LE8->clear();
          ui->LE9->clear();
          ui->LE10->clear();
      }
}
