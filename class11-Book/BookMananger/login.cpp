﻿#include "login.h"
#include "ui_login.h"
#include <QMessageBox>
#pragma execution_character_set("utf-8");
Login::Login(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Login)
{
    ui->setupUi(this);
    ui->userNameLEd->setPlaceholderText(tr("请输入用户名!"));
    ui->pwdLEd->setPlaceholderText("请输入密码!");
    connect(ui->loginBtn,&QPushButton::clicked,this,&Login::login);
    connect(ui->exitBtn,&QPushButton::clicked,this,&Login::close);
}

Login::~Login()
{
    delete ui;
}
void Login::login()
{
    //trimmed()去掉前后空格
    //tr()函数，防止设置中文时乱码
    if(ui->userNameLEd->text().trimmed() == tr("123") && ui->pwdLEd->text() == tr("123"))
    {
       QMessageBox::information(this,"info","登录成功");
       accept();//关闭窗体，并设置返回值为Accepted
    }
    else
    {
       QMessageBox::warning(this, tr("警告！"),tr("用户名或密码错误！"),QMessageBox::Yes);

    // 清空输入框内容
       ui->userNameLEd->clear();
       ui->pwdLEd->clear();

       //光标定位
       ui->userNameLEd->setFocus();
    }
}

