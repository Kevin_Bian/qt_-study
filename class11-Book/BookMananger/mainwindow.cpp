﻿#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "login.h"
#include "ui_login.h"
#include <QMessageBox>
#include <QSql>
#include <QDialog>
#include <QDebug>
#include <QMessageBox>
#include <QSqlError>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlQueryModel>
#include <QFont>
#include "addbook.h"
#include "vip.h"
int i=0;double money=0;double temp;double money2;int left2;int temp2;

#pragma execution_character_set("utf-8");

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    //SQL连接
    QSqlDatabase db = QSqlDatabase::addDatabase("QODBC");
    qDebug()<<"ODBC driver?"<<db.isValid();
    QString dsn = QString::fromLocal8Bit("BookStore");
    db.setHostName("localhost");
    db.setDatabaseName(dsn);
    db.setUserName("sa");                               //登录用户
    db.setPassword("BB.WW.KK");
    db.open();//打开数据库
    ui->setupUi(this);
    setWindowTitle("书店管理系统");
 //   ui->PicLAB->setPixmap(QPixmap(":/image/书店.jpg"));
    ui->stackedWidget->setCurrentIndex(0);
    QSqlQueryModel *model = new QSqlQueryModel(ui->tableView);
    QSqlQueryModel *model2 = new QSqlQueryModel(ui->tableView2);
    QSqlQueryModel *model3 = new QSqlQueryModel(ui->VIPview);
    QSqlQueryModel *model4 = new QSqlQueryModel(ui->tableView_2);
    QSqlQueryModel *model5 = new QSqlQueryModel(ui->tableView2);
    QSqlQueryModel *model6 = new QSqlQueryModel(ui->tableView_3);
    QSqlQueryModel *model7 = new QSqlQueryModel(ui->tableView_4);
    connect(ui->VIPLE3,&QLineEdit::textChanged,[=](){
        QString sql = "select * from VIP where name LIKE '%" + ui->VIPLE3->text() + "%'";
        model5->setQuery(sql);
        model5->setHeaderData(0,Qt::Horizontal,QObject::tr("姓名"));
        model5->setHeaderData(1,Qt::Horizontal,QObject::tr("电话"));
        model5->setHeaderData(2,Qt::Horizontal,QObject::tr("注册时间"));
        ui->tableView2->setModel(model5);
    });
    connect(ui->VIPLE4,&QLineEdit::textChanged,[=](){
        QString sql = "select * from VIP where phoneNum LIKE '%" + ui->VIPLE4->text() + "%'";
        model5->setQuery(sql);
        model5->setHeaderData(0,Qt::Horizontal,QObject::tr("姓名"));
        model5->setHeaderData(1,Qt::Horizontal,QObject::tr("电话"));
        model5->setHeaderData(2,Qt::Horizontal,QObject::tr("注册时间"));
        ui->tableView2->setModel(model5);
    });
    connect(ui->EM,&QPushButton::clicked,[=](){
        ui->stackedWidget->setCurrentIndex(4);
        QString sql = "select * from employee";
        model7->setQuery(sql);
            model7->setHeaderData(0,Qt::Horizontal,QObject::tr("姓名"));
            model7->setHeaderData(1,Qt::Horizontal,QObject::tr("职务"));
            ui->tableView_4->setModel(model7);
    });
    connect(ui->Dadd,&QPushButton::clicked,[=](){
          QSqlQuery query7(db);
          query7.prepare("INSERT INTO employee values (:name,:position)");
          query7.bindValue(0,ui->lineEdit111->text());
          query7.bindValue(1,ui->lineEdit222->text());
          if(query7.exec()==QDialog::Accepted)
          {
              QMessageBox::information(this,tr("info"),tr("插入成功"));
              //清除数据
              ui->lineEdit111->clear();
              ui->lineEdit222->clear();
          }
          else
          {
              QMessageBox::warning(this, tr("警告！"),tr("插入错误！"),QMessageBox::Yes);
              ui->lineEdit111->clear();
              ui->lineEdit222->clear();
          }

    });
    connect(ui->DSX,&QPushButton::clicked,[=](){
        QString sql = "select * from employee";
        model7->setQuery(sql);
            model7->setHeaderData(0,Qt::Horizontal,QObject::tr("姓名"));
            model7->setHeaderData(1,Qt::Horizontal,QObject::tr("职务"));
            ui->tableView_4->setModel(model7);
    });
    connect(ui->Ddel,&QPushButton::clicked,[=](){
        QSqlQuery query3(db);
        if(!ui->lineEdit111->text().isEmpty())
        {
            query3.prepare("delete from employee where name = :name");
            query3.bindValue(":name",ui->lineEdit111->text());
        }
        else if(!ui->lineEdit222->text().isEmpty())
        {
            query3.prepare("delete from employee where position = :ISBN");
            query3.bindValue(":ISBN",ui->lineEdit222->text());
        }
        if(query3.exec()==QDialog::Accepted)
        {
            QMessageBox::information(this,tr("info"),tr("删除成功"));
            //清除数据
            ui->lineEdit111->clear();
            ui->lineEdit222->clear();
        }
        else
        {
            QMessageBox::warning(this, tr("警告！"),tr("删除失败！"),QMessageBox::Yes);
            ui->lineEdit111->clear();
            ui->lineEdit222->clear();
        }
    });
    connect(ui->XSB1,&QPushButton::clicked,[=](){
         ui->stackedWidget->setCurrentIndex(1);
         QString sql = "select * from sell order by date DESC";
         model6->setQuery(sql);
             model6->setHeaderData(0,Qt::Horizontal,QObject::tr("书名"));
             model6->setHeaderData(1,Qt::Horizontal,QObject::tr("ISBN"));
             model6->setHeaderData(2,Qt::Horizontal,QObject::tr("价格"));
             model6->setHeaderData(3,Qt::Horizontal,QObject::tr("数量"));
             model6->setHeaderData(4,Qt::Horizontal,QObject::tr("剩余数量"));
             model6->setHeaderData(5,Qt::Horizontal,QObject::tr("会员姓名"));
             model6->setHeaderData(6,Qt::Horizontal,QObject::tr("会员电话"));
             model6->setHeaderData(7,Qt::Horizontal,QObject::tr("购买时间"));
             model6->setHeaderData(8,Qt::Horizontal,QObject::tr("总金额"));
             ui->tableView_3->setModel(model6);
    });
    connect(ui->LE00,&QLineEdit::textChanged,[=](){
            QString sql = "select * from sell where name LIKE '%" + ui->LE00->text() + "%'";
            model6->setQuery(sql);
                model6->setHeaderData(0,Qt::Horizontal,QObject::tr("书名"));
                model6->setHeaderData(1,Qt::Horizontal,QObject::tr("ISBN"));
                model6->setHeaderData(2,Qt::Horizontal,QObject::tr("价格"));
                model6->setHeaderData(3,Qt::Horizontal,QObject::tr("数量"));
                model6->setHeaderData(4,Qt::Horizontal,QObject::tr("剩余数量"));
                model6->setHeaderData(5,Qt::Horizontal,QObject::tr("会员姓名"));
                model6->setHeaderData(6,Qt::Horizontal,QObject::tr("会员电话"));
                model6->setHeaderData(7,Qt::Horizontal,QObject::tr("购买时间"));
                model6->setHeaderData(8,Qt::Horizontal,QObject::tr("总金额"));
                ui->tableView_3->setModel(model6);
    });
    connect(ui->LE11,&QLineEdit::textChanged,[=](){
        QString sql = "select * from sell where VIP1 LIKE '%" + ui->LE11->text() + "%'";
        model6->setQuery(sql);
            model6->setHeaderData(0,Qt::Horizontal,QObject::tr("书名"));
            model6->setHeaderData(1,Qt::Horizontal,QObject::tr("ISBN"));
            model6->setHeaderData(2,Qt::Horizontal,QObject::tr("价格"));
            model6->setHeaderData(3,Qt::Horizontal,QObject::tr("数量"));
            model6->setHeaderData(4,Qt::Horizontal,QObject::tr("剩余数量"));
            model6->setHeaderData(5,Qt::Horizontal,QObject::tr("会员姓名"));
            model6->setHeaderData(6,Qt::Horizontal,QObject::tr("会员电话"));
            model6->setHeaderData(7,Qt::Horizontal,QObject::tr("购买时间"));
            model6->setHeaderData(8,Qt::Horizontal,QObject::tr("总金额"));
            ui->tableView_3->setModel(model6);
    });
    connect(ui->LE22,&QLineEdit::textChanged,[=](){
        QString sql = "select * from sell where date LIKE '%" + ui->LE22->text() + "%'";
        model6->setQuery(sql);
            model6->setHeaderData(0,Qt::Horizontal,QObject::tr("书名"));
            model6->setHeaderData(1,Qt::Horizontal,QObject::tr("ISBN"));
            model6->setHeaderData(2,Qt::Horizontal,QObject::tr("价格"));
            model6->setHeaderData(3,Qt::Horizontal,QObject::tr("数量"));
            model6->setHeaderData(4,Qt::Horizontal,QObject::tr("剩余数量"));
            model6->setHeaderData(5,Qt::Horizontal,QObject::tr("会员姓名"));
            model6->setHeaderData(6,Qt::Horizontal,QObject::tr("会员电话"));
            model6->setHeaderData(7,Qt::Horizontal,QObject::tr("购买时间"));
            model6->setHeaderData(8,Qt::Horizontal,QObject::tr("总金额"));
            ui->tableView_3->setModel(model6);
    });
    connect(ui->TC1,&QPushButton::clicked,this,&MainWindow::close);
    //零售
    connect(ui->LSBTN1,&QPushButton::clicked,[=](){
        ui->stackedWidget->setCurrentIndex(0);
    });
    connect(ui->VIPLE0,&QLineEdit::textChanged,[=](){
        QString sql = "select * from vip where name LIKE '%" + ui->VIPLE0->text() + "%'";
        model3->setQuery(sql);
            model3->setHeaderData(0,Qt::Horizontal,QObject::tr("姓名"));
            model3->setHeaderData(1,Qt::Horizontal,QObject::tr("电话号码"));
            model3->setHeaderData(2,Qt::Horizontal,QObject::tr("注册时间"));
            ui->VIPview->setModel(model3);
    });
    connect(ui->VIPLE1,&QLineEdit::textChanged,[=](){
        QString sql = "select * from vip where phoneNum LIKE '" + ui->VIPLE1->text() + "'";
        model3->setQuery(sql);
            model3->setHeaderData(0,Qt::Horizontal,QObject::tr("姓名"));
            model3->setHeaderData(1,Qt::Horizontal,QObject::tr("电话号码"));
            model3->setHeaderData(2,Qt::Horizontal,QObject::tr("注册时间"));
            ui->VIPview->setModel(model3);

    });
    connect(ui->BLE0,&QLineEdit::textChanged,[=](){
        QString sql = "select * from book where name LIKE '%" + ui->BLE0->text() + "%'";
        model4->setQuery(sql);
            model4->setHeaderData(0,Qt::Horizontal,QObject::tr("书名"));
            model4->setHeaderData(1,Qt::Horizontal,QObject::tr("作者"));
            model4->setHeaderData(2,Qt::Horizontal,QObject::tr("价格"));
            model4->setHeaderData(3,Qt::Horizontal,QObject::tr("ISBN"));
            model4->setHeaderData(4,Qt::Horizontal,QObject::tr("出版社"));
            model4->setHeaderData(5,Qt::Horizontal,QObject::tr("语言"));
            model4->setHeaderData(6,Qt::Horizontal,QObject::tr("页数"));
            model4->setHeaderData(7,Qt::Horizontal,QObject::tr("包装"));
            model4->setHeaderData(8,Qt::Horizontal,QObject::tr("出版时间"));
            model4->setHeaderData(9,Qt::Horizontal,QObject::tr("版次"));
            model4->setHeaderData(10,Qt::Horizontal,QObject::tr("库存"));
            ui->tableView_2->setModel(model4);
    });
    connect(ui->SX1,&QPushButton::clicked,[=](){
        if(!ui->Money1->text().isEmpty())
        {
            if(!ui->VIPLE0->text().isEmpty())
            {
                 money2 = money * 0.9;
            }
            else if(!ui->VIPLE1->text().isEmpty())
            {
                money2 = money * 0.9;
            }
            ui->Money2->setText(QString::number(money2,10,2));
            }
        else
        {
            QMessageBox::warning(this, tr("警告！"),tr("无目标金额！"),QMessageBox::Yes);
        }
    });
    ui->tableWidget->setColumnCount(5);
    ui->tableWidget->setHorizontalHeaderLabels(QStringList()<<"书名"<<"ISBN"<<"价格"<<"数量"<<"剩余库存");
    ui->tableWidget->setRowCount(100);
    connect(ui->Ladd1,&QPushButton::clicked,[=](){
        QSqlQuery queryLS = model4->query();
        QSqlQuery queryLS2 = model4->query();
     if(ui->BLE0->text().isEmpty())
        {
            QMessageBox::warning(this, tr("警告！"),tr("输入书名！"),QMessageBox::Yes);
        }
     else
     {
        if(!ui->BLE2->text().isEmpty())
        {
        queryLS.exec("select * from book where name LIKE '%" + ui->BLE0->text() + "%'");
        while(queryLS.next())
        {
            left2 = queryLS.value(10).toInt() - ui->BLE2->text().toInt();
            if(left2 < 0)
            {
                QMessageBox::warning(this, tr("警告！"),tr("库存不足！"),QMessageBox::Yes);
                break;
            }
            temp = queryLS.value(2).toDouble() * ui->BLE2->text().toInt();
            money += temp;
            int col=0;
            ui->tableWidget->setItem(i,col++,new QTableWidgetItem(queryLS.value(0).toString()));
            ui->tableWidget->setItem(i,col++,new QTableWidgetItem(queryLS.value(3).toString()));
            ui->tableWidget->setItem(i,col++,new QTableWidgetItem(queryLS.value(2).toString()));
            ui->tableWidget->setItem(i,col++,new QTableWidgetItem(ui->BLE2->text()));
            ui->tableWidget->setItem(i,col++,new QTableWidgetItem(QString::number(left2)));
            ++i;
        }
        ui->Money1->setText(QString::number(money,10,2));
        if(!ui->VIPLE0->text().isEmpty())
        {
             money2 = money * 0.9;
        }
        else if(!ui->VIPLE1->text().isEmpty())
        {
            money2 = money * 0.9;
        }
        ui->Money2->setText(QString::number(money2,10,2));
        ui->BLE0->clear();
        ui->BLE1->clear();
        ui->BLE2->setText("1");
        }
        else
        {
            QMessageBox::warning(this, tr("警告！"),tr("未输入数量"),QMessageBox::Yes);
        }
    }
    });
    connect(ui->Ldel1,&QPushButton::clicked,[=](){
        --i;
        int col=0;

        money-=temp;

        ui->Money1->setText(QString::number(money,10,2));

        if(!ui->VIPLE0->text().isEmpty())
        {
             money2 = money * 0.9;
        }
        else if(!ui->VIPLE1->text().isEmpty())
        {
            money2 = money * 0.9;
        }
        ui->Money2->setText(QString::number(money2,10,2));
        ui->tableWidget->setItem(i,col++,new QTableWidgetItem(" "));
        ui->tableWidget->setItem(i,col++,new QTableWidgetItem(" "));
        ui->tableWidget->setItem(i,col++,new QTableWidgetItem(" "));
        ui->tableWidget->setItem(i,col++,new QTableWidgetItem(" "));
        ui->tableWidget->setItem(i,col++,new QTableWidgetItem(" "));
    });
    connect(ui->tableBTN1,&QPushButton::clicked,[=](){
        QSqlQueryModel *model9 = new QSqlQueryModel(ui->tableView_2);
        model9->setQuery("SELECT * FROM book");
            model9->setHeaderData(0,Qt::Horizontal,QObject::tr("书名"));
            model9->setHeaderData(1,Qt::Horizontal,QObject::tr("作者"));
            model9->setHeaderData(2,Qt::Horizontal,QObject::tr("价格"));
            model9->setHeaderData(3,Qt::Horizontal,QObject::tr("ISBN"));
            model9->setHeaderData(4,Qt::Horizontal,QObject::tr("出版社"));
            model9->setHeaderData(5,Qt::Horizontal,QObject::tr("语言"));
            model9->setHeaderData(6,Qt::Horizontal,QObject::tr("页数"));
            model9->setHeaderData(7,Qt::Horizontal,QObject::tr("包装"));
            model9->setHeaderData(8,Qt::Horizontal,QObject::tr("出版时间"));
            model9->setHeaderData(9,Qt::Horizontal,QObject::tr("版次"));
            model9->setHeaderData(10,Qt::Horizontal,QObject::tr("库存"));
            ui->tableView_2->setModel(model9);
    });
    connect(ui->qingkong1,&QPushButton::clicked,[=](){
        ui->BLE0->clear();
        ui->BLE1->clear();
        ui->VIPLE0->clear();
        ui->VIPLE1->clear();
        ui->Money1->clear();
        ui->Money2->clear();
        temp =0;
        money=0;
        temp2=0;
        money2=0;
        left2=0;
        while(i>0)
        {
           --i;
        int col=0;
        ui->tableWidget->setItem(i,col++,new QTableWidgetItem(" "));
        ui->tableWidget->setItem(i,col++,new QTableWidgetItem(" "));
        ui->tableWidget->setItem(i,col++,new QTableWidgetItem(" "));
        ui->tableWidget->setItem(i,col++,new QTableWidgetItem(" "));
        ui->tableWidget->setItem(i,col++,new QTableWidgetItem(" "));

        }
    });
    connect(ui->JS1,&QPushButton::clicked,[=](){
        QSqlQuery queryLS3 = model4->query();
        qDebug()<<ui->Money2->text().toInt();
        if(!ui->VIPLE0->text().isEmpty())
        QMessageBox::information(this,"info","结算成功总计:"+ui->Money2->text()+"元");
        else
        QMessageBox::information(this,"info","结算成功总计:"+ui->Money1->text()+"元");
        QSqlQuery queryLS33 = model4->query();
        while(i>0)
        {
           --i;
           queryLS33.prepare("update book set num = :num where name = :name");
           queryLS33.bindValue(":name",ui->tableWidget->item(i,0)->text());
           queryLS33.bindValue(":num",ui->tableWidget->item(i,4)->text().toInt());
           qDebug() << ui->tableWidget->item(i,4)->text().toInt();
           queryLS3.prepare("insert into sell (name,ISBN,price,num,leftnum,VIP1,VIP2,total) values(:name,:ISBN,:price,:num,:leftnum,:VIP1,:VIP2,:total)");
           queryLS3.bindValue(0,ui->tableWidget->item(i,0)->text());
           queryLS3.bindValue(1,ui->tableWidget->item(i,1)->text());
           queryLS3.bindValue(2,ui->tableWidget->item(i,2)->text());
           queryLS3.bindValue(3,ui->tableWidget->item(i,3)->text());
           queryLS3.bindValue(4,ui->tableWidget->item(i,4)->text());
           if(!ui->VIPLE0->text().isEmpty())
           queryLS3.bindValue(5,ui->VIPLE0->text());
           else
           queryLS3.bindValue(5,"零售");
           if(!ui->VIPLE1->text().isEmpty())
           queryLS3.bindValue(6,ui->VIPLE1->text());
           else
           queryLS3.bindValue(6,"零售");
           if(ui->VIPLE0->text().isEmpty())
           queryLS3.bindValue(7,ui->Money1->text().toDouble());
           else
           queryLS3.bindValue(7,ui->Money2->text().toDouble());
           queryLS3.exec();
           queryLS33.exec();
        int col=0;
        ui->tableWidget->setItem(i,col++,new QTableWidgetItem(" "));
        ui->tableWidget->setItem(i,col++,new QTableWidgetItem(" "));
        ui->tableWidget->setItem(i,col++,new QTableWidgetItem(" "));
        ui->tableWidget->setItem(i,col++,new QTableWidgetItem(" "));
        ui->tableWidget->setItem(i,col++,new QTableWidgetItem(" "));
        }
//        queryLS3.prepare("insert into sell values(:name,:ISBN,:price,:num,:leftnum,:VIP1,:VIP2,:date,:total)");
//        queryLS3.bindValue(0," ");
//        queryLS3.bindValue(1," ");
//        queryLS3.bindValue(2," ");
//        queryLS3.bindValue(3," ");
//        queryLS3.bindValue(4," ");
//        queryLS3.bindValue(5," ");
//        queryLS3.bindValue(6," ");
//        queryLS3.bindValue(7," ");
//        queryLS3.bindValue(8," ");
//        queryLS3.exec();
        temp =0;
        money=0;
        temp2=0;
        money2=0;
        left2=0;
        ui->BLE0->clear();
        ui->BLE1->clear();
        ui->VIPLE0->clear();
        ui->VIPLE1->clear();
        ui->Money1->clear();
        ui->Money2->clear();
    });
    connect(ui->BLE1,&QLineEdit::textChanged,[=](){
        QString sql = "select * from book where ISBN LIKE '%" + ui->BLE1->text() + "%'";
        model4->setQuery(sql);
            model4->setHeaderData(0,Qt::Horizontal,QObject::tr("书名"));
            model4->setHeaderData(1,Qt::Horizontal,QObject::tr("作者"));
            model4->setHeaderData(2,Qt::Horizontal,QObject::tr("价格"));
            model4->setHeaderData(3,Qt::Horizontal,QObject::tr("ISBN"));
            model4->setHeaderData(4,Qt::Horizontal,QObject::tr("出版社"));
            model4->setHeaderData(5,Qt::Horizontal,QObject::tr("语言"));
            model4->setHeaderData(6,Qt::Horizontal,QObject::tr("页数"));
            model4->setHeaderData(7,Qt::Horizontal,QObject::tr("包装"));
            model4->setHeaderData(8,Qt::Horizontal,QObject::tr("出版时间"));
            model4->setHeaderData(9,Qt::Horizontal,QObject::tr("版次"));
            model4->setHeaderData(10,Qt::Horizontal,QObject::tr("库存"));
            ui->tableView_2->setModel(model4);
    });  
    connect(ui->KuBTN1,&QPushButton::clicked,this,&MainWindow::showInfo);
    connect(ui->VIPBTN1,&QPushButton::clicked,this,&MainWindow::showVip);
    connect(ui->addBTN1,&QPushButton::clicked,[=](){
        addBook* add = new addBook(this);
        add->show();
        add->setAttribute(Qt::WA_DeleteOnClose);
    });
    connect(ui->VIPBTN2,&QPushButton::clicked,[=](){
         VIP* Vip = new VIP(this);
         Vip->show();
         Vip->setAttribute(Qt::WA_DeleteOnClose);
    });
    connect(ui->Query1,&QLineEdit::textChanged,[=](){
        QString sql = "select * from book where name LIKE '%" + ui->Query1->text() + "%'";
        model->setQuery(sql);
        model->setHeaderData(0,Qt::Horizontal,QObject::tr("书名"));
        model->setHeaderData(1,Qt::Horizontal,QObject::tr("作者"));
        model->setHeaderData(2,Qt::Horizontal,QObject::tr("价格"));
        model->setHeaderData(3,Qt::Horizontal,QObject::tr("ISBN"));
        model->setHeaderData(4,Qt::Horizontal,QObject::tr("出版社"));
        model->setHeaderData(5,Qt::Horizontal,QObject::tr("语言"));
        model->setHeaderData(6,Qt::Horizontal,QObject::tr("页数"));
        model->setHeaderData(7,Qt::Horizontal,QObject::tr("包装"));
        model->setHeaderData(8,Qt::Horizontal,QObject::tr("出版时间"));
        model->setHeaderData(9,Qt::Horizontal,QObject::tr("版次"));
        model->setHeaderData(10,Qt::Horizontal,QObject::tr("库存"));
        ui->tableView->setModel(model);
    });
    connect(ui->Query2,&QLineEdit::textChanged,[=](){
        QString sql = "select * from book where author LIKE '%" + ui->Query2->text() + "%'";
        model->setQuery(sql);
        model->setHeaderData(0,Qt::Horizontal,QObject::tr("书名"));
        model->setHeaderData(1,Qt::Horizontal,QObject::tr("作者"));
        model->setHeaderData(2,Qt::Horizontal,QObject::tr("价格"));
        model->setHeaderData(3,Qt::Horizontal,QObject::tr("ISBN"));
        model->setHeaderData(4,Qt::Horizontal,QObject::tr("出版社"));
        model->setHeaderData(5,Qt::Horizontal,QObject::tr("语言"));
        model->setHeaderData(6,Qt::Horizontal,QObject::tr("页数"));
        model->setHeaderData(7,Qt::Horizontal,QObject::tr("包装"));
        model->setHeaderData(8,Qt::Horizontal,QObject::tr("出版时间"));
        model->setHeaderData(9,Qt::Horizontal,QObject::tr("版次"));
        model->setHeaderData(10,Qt::Horizontal,QObject::tr("库存"));
        ui->tableView->setModel(model);
    });
    connect(ui->Query3,&QLineEdit::textChanged,[=](){
        QString sql = "select * from book where ISBN LIKE '%" + ui->Query3->text() + "%'";
        model->setQuery(sql);
        model->setHeaderData(0,Qt::Horizontal,QObject::tr("书名"));
        model->setHeaderData(1,Qt::Horizontal,QObject::tr("作者"));
        model->setHeaderData(2,Qt::Horizontal,QObject::tr("价格"));
        model->setHeaderData(3,Qt::Horizontal,QObject::tr("ISBN"));
        model->setHeaderData(4,Qt::Horizontal,QObject::tr("出版社"));
        model->setHeaderData(5,Qt::Horizontal,QObject::tr("语言"));
        model->setHeaderData(6,Qt::Horizontal,QObject::tr("页数"));
        model->setHeaderData(7,Qt::Horizontal,QObject::tr("包装"));
        model->setHeaderData(8,Qt::Horizontal,QObject::tr("出版时间"));
        model->setHeaderData(9,Qt::Horizontal,QObject::tr("版次"));
        model->setHeaderData(10,Qt::Horizontal,QObject::tr("库存"));
        ui->tableView->setModel(model);
    });
    connect(ui->SXBTN1,&QPushButton::clicked,this,&MainWindow::showInfo);
}
void MainWindow::showInfo2()
{
    QSqlDatabase db11 = QSqlDatabase::addDatabase("QODBC");
    qDebug()<<"ODBC driver?"<<db11.isValid();//数据库驱动类型为SQL Server
    QString dsn = QString::fromLocal8Bit("BookStore");      //数据源名称
    db11.setHostName("localhost");                        //选择本地主机，127.0.1.1
    db11.setDatabaseName(dsn);                            //设置数据源名称
    db11.setUserName("sa");                               //登录用户
    db11.setPassword("BB.WW.KK");
    db11.open();//打开数据库
    QSqlQueryModel *model5 = new QSqlQueryModel(ui->tableView_2);
    model5->setQuery("SELECT * FROM book");
        model5->setHeaderData(0,Qt::Horizontal,QObject::tr("书名"));
        model5->setHeaderData(1,Qt::Horizontal,QObject::tr("作者"));
        model5->setHeaderData(2,Qt::Horizontal,QObject::tr("价格"));
        model5->setHeaderData(3,Qt::Horizontal,QObject::tr("ISBN"));
        model5->setHeaderData(4,Qt::Horizontal,QObject::tr("出版社"));
        model5->setHeaderData(5,Qt::Horizontal,QObject::tr("语言"));
        model5->setHeaderData(6,Qt::Horizontal,QObject::tr("页数"));
        model5->setHeaderData(7,Qt::Horizontal,QObject::tr("包装"));
        model5->setHeaderData(8,Qt::Horizontal,QObject::tr("出版时间"));
        model5->setHeaderData(9,Qt::Horizontal,QObject::tr("版次"));
        model5->setHeaderData(10,Qt::Horizontal,QObject::tr("库存"));
        ui->tableView_2->setModel(model5);
}
void MainWindow::showVip()
{
    QSqlDatabase db33 = QSqlDatabase::addDatabase("QODBC");
    qDebug()<<"ODBC driver?"<<db33.isValid();//数据库驱动类型为SQL Server
    QString dsn = QString::fromLocal8Bit("BookStore");      //数据源名称
    db33.setHostName("localhost");                        //选择本地主机，127.0.1.1
    db33.setDatabaseName(dsn);                            //设置数据源名称
    db33.setUserName("sa");                               //登录用户
    db33.setPassword("BB.WW.KK");
    db33.open();//打开数据库
    QSqlQueryModel *model2 = new QSqlQueryModel(ui->tableView2);
    ui->stackedWidget->setCurrentIndex(3);
    model2->setQuery("SELECT * FROM VIP");
        model2->setHeaderData(0,Qt::Horizontal,QObject::tr("姓名"));
        model2->setHeaderData(1,Qt::Horizontal,QObject::tr("电话号码"));
        model2->setHeaderData(2,Qt::Horizontal,QObject::tr("注册时间"));
        ui->tableView2->setModel(model2);
}
void MainWindow::showInfo()
{
    QSqlDatabase db55 = QSqlDatabase::addDatabase("QODBC");
    qDebug()<<"ODBC driver?"<<db55.isValid();//数据库驱动类型为SQL Server
    QString dsn = QString::fromLocal8Bit("BookStore");      //数据源名称
    db55.setHostName("localhost");                        //选择本地主机，127.0.1.1
    db55.setDatabaseName(dsn);                            //设置数据源名称
    db55.setUserName("sa");                               //登录用户
    db55.setPassword("BB.WW.KK");
    db55.open();//打开数据库
    QSqlQueryModel *model = new QSqlQueryModel(ui->tableView);
    ui->stackedWidget->setCurrentIndex(2);
    model->setQuery("SELECT * FROM book");
        model->setHeaderData(0,Qt::Horizontal,QObject::tr("书名"));
        model->setHeaderData(1,Qt::Horizontal,QObject::tr("作者"));
        model->setHeaderData(2,Qt::Horizontal,QObject::tr("价格"));
        model->setHeaderData(3,Qt::Horizontal,QObject::tr("ISBN"));
        model->setHeaderData(4,Qt::Horizontal,QObject::tr("出版社"));
        model->setHeaderData(5,Qt::Horizontal,QObject::tr("语言"));
        model->setHeaderData(6,Qt::Horizontal,QObject::tr("页数"));
        model->setHeaderData(7,Qt::Horizontal,QObject::tr("包装"));
        model->setHeaderData(8,Qt::Horizontal,QObject::tr("出版时间"));
        model->setHeaderData(9,Qt::Horizontal,QObject::tr("版次"));
        model->setHeaderData(10,Qt::Horizontal,QObject::tr("库存"));
        ui->tableView->setModel(model);
}
MainWindow::~MainWindow()
{
    delete ui;
}
