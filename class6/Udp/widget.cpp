#include "widget.h"
#include "ui_widget.h"
#include <QHostAddress>

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
    udpSocket = new QUdpSocket(this);
}

Widget::~Widget()
{
    delete ui;
}

void Widget::on_openButton_clicked()
{
    udpSocket->bind(ui->localEdit->text().toUInt());
    connect(udpSocket,SIGNAL(readyRead()),this, SLOT(readyRead_slot()));
}

void Widget::readyRead_slot()
{
    while (udpSocket->hasPendingDatagrams())
    {
        QByteArray arry;
        arry.resize(udpSocket->pendingDatagramSize());
        udpSocket->readDatagram(arry.data(),arry.size());
        ui->receiveEdit->appendPlainText(arry.data());
    }
}

void Widget::on_sendButton_clicked()
{
    quint16 port;
    QHostAddress address;
    QString buf = ui->ipEdit->text();
    address.setAddress(buf);
    QString str = ui->sendEdit->text();
    port = ui->aimEdit->text().toInt();
    udpSocket->writeDatagram(str.toUtf8(),buf.length(),address,port);
}

void Widget::on_closeButton_clicked()
{
    udpSocket->close();
}
