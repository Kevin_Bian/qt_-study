#include "widget.h"
#include "ui_widget.h"
#include <QHostAddress>
#include <QString>

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
    udpSocket = new QUdpSocket (this);

}

Widget::~Widget()
{
    delete ui;
}
void Widget::on_openButton_clicked()
{
    udpSocket->bind(ui->localPort->text().toUInt());
    connect(udpSocket,SIGNAL(readyRead()),this, SLOT(readyRead_slot()));
}

void Widget::readyRead_slot()
{
    while (udpSocket->hasPendingDatagrams())
    {
        QByteArray arry;
        arry.resize(udpSocket->pendingDatagramSize());
        udpSocket->readDatagram(arry.data(),arry.size());
        ui->receiveEdit->appendPlainText(arry.data());
    }
}

void Widget::on_sendButton_clicked()
{
    quint16 port;
    QHostAddress address;
    QString buf = ui->ipEdit->text();
    address.setAddress(buf);
    QString str = ui->sendEdit->text();
    port = ui->aimPort->text().toInt();
    udpSocket->writeDatagram(str.toUtf8(),buf.length(),address,port);
}

void Widget::on_closeButton_clicked()
{
    udpSocket->close();
}

/*
void Widget::on_openButton_clicked()
{
    udpSocket->bind(ui->localPort->text().toUInt()); //绑定本地端口
    connect(udpSocket,SIGNAL(readyRead()),this,SLOT(readyRead_slot()));  //当对方成功发送过来数据时，触发readyRead()信号

}
void Widget::readyRead_slot()
{
    //读取对方发送的内容
    char buf[1024]={0};
    QHostAddress addr;
    quint16 port;
    qint64 len= udpSocket->readDatagram(buf,sizeof(buf),&addr,&port);
    if(len>0)
    {
        QString str = QString("[%1:%2]%3").arg(addr.toString()).arg(port).arg(buf);
        ui->receiveEdit->appendPlainText(str);
    }
}

void Widget::on_closeButton_clicked()
{
    udpSocket->close();
}

void Widget::on_sendButton_clicked()
{
    QString ip=ui->ipEdit->text();
    quint16 port = ui->aimPort->text().toInt();
    QString str = ui->sendEdit->text();
    udpSocket->writeDatagram(str.toUtf8(),QHostAddress(ip),port);

}
*/
