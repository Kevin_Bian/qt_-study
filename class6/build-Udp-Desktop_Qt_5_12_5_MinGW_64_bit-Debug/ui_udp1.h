/********************************************************************************
** Form generated from reading UI file 'udp1.ui'
**
** Created by: Qt User Interface Compiler version 5.12.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_UDP1_H
#define UI_UDP1_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Udp1
{
public:
    QGridLayout *gridLayout;
    QGroupBox *groupBox;
    QPlainTextEdit *plainTextEdit;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label;
    QLineEdit *localPort;
    QLabel *label_2;
    QLineEdit *aimPort;
    QHBoxLayout *horizontalLayout_4;
    QHBoxLayout *horizontalLayout;
    QLabel *label_3;
    QLineEdit *ipEdit;
    QSpacerItem *horizontalSpacer_3;
    QGroupBox *groupBox_2;
    QLineEdit *lineEdit;
    QHBoxLayout *horizontalLayout_3;
    QPushButton *openButton;
    QSpacerItem *horizontalSpacer;
    QPushButton *closeButton;
    QSpacerItem *horizontalSpacer_2;
    QPushButton *pushButton_3;

    void setupUi(QWidget *Udp1)
    {
        if (Udp1->objectName().isEmpty())
            Udp1->setObjectName(QString::fromUtf8("Udp1"));
        Udp1->resize(471, 474);
        gridLayout = new QGridLayout(Udp1);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        groupBox = new QGroupBox(Udp1);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));

        gridLayout->addWidget(groupBox, 0, 0, 1, 1);

        plainTextEdit = new QPlainTextEdit(Udp1);
        plainTextEdit->setObjectName(QString::fromUtf8("plainTextEdit"));

        gridLayout->addWidget(plainTextEdit, 1, 0, 1, 1);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label = new QLabel(Udp1);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout_2->addWidget(label);

        localPort = new QLineEdit(Udp1);
        localPort->setObjectName(QString::fromUtf8("localPort"));

        horizontalLayout_2->addWidget(localPort);

        label_2 = new QLabel(Udp1);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        horizontalLayout_2->addWidget(label_2);

        aimPort = new QLineEdit(Udp1);
        aimPort->setObjectName(QString::fromUtf8("aimPort"));

        horizontalLayout_2->addWidget(aimPort);


        gridLayout->addLayout(horizontalLayout_2, 2, 0, 1, 1);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label_3 = new QLabel(Udp1);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        horizontalLayout->addWidget(label_3);

        ipEdit = new QLineEdit(Udp1);
        ipEdit->setObjectName(QString::fromUtf8("ipEdit"));
        ipEdit->setMinimumSize(QSize(153, 0));

        horizontalLayout->addWidget(ipEdit);


        horizontalLayout_4->addLayout(horizontalLayout);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_3);


        gridLayout->addLayout(horizontalLayout_4, 3, 0, 1, 1);

        groupBox_2 = new QGroupBox(Udp1);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));

        gridLayout->addWidget(groupBox_2, 4, 0, 1, 1);

        lineEdit = new QLineEdit(Udp1);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));

        gridLayout->addWidget(lineEdit, 5, 0, 1, 1);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        openButton = new QPushButton(Udp1);
        openButton->setObjectName(QString::fromUtf8("openButton"));

        horizontalLayout_3->addWidget(openButton);

        horizontalSpacer = new QSpacerItem(90, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer);

        closeButton = new QPushButton(Udp1);
        closeButton->setObjectName(QString::fromUtf8("closeButton"));

        horizontalLayout_3->addWidget(closeButton);

        horizontalSpacer_2 = new QSpacerItem(91, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_2);

        pushButton_3 = new QPushButton(Udp1);
        pushButton_3->setObjectName(QString::fromUtf8("pushButton_3"));

        horizontalLayout_3->addWidget(pushButton_3);


        gridLayout->addLayout(horizontalLayout_3, 6, 0, 1, 1);


        retranslateUi(Udp1);

        QMetaObject::connectSlotsByName(Udp1);
    } // setupUi

    void retranslateUi(QWidget *Udp1)
    {
        Udp1->setWindowTitle(QApplication::translate("Udp1", "Form", nullptr));
        groupBox->setTitle(QApplication::translate("Udp1", "\346\216\245\346\224\266\346\241\206", nullptr));
        label->setText(QApplication::translate("Udp1", "\346\234\254\345\234\260\347\253\257\345\217\243", nullptr));
        label_2->setText(QApplication::translate("Udp1", "\347\233\256\346\240\207\347\253\257\345\217\243", nullptr));
        label_3->setText(QApplication::translate("Udp1", "\347\233\256\346\240\207IP", nullptr));
        groupBox_2->setTitle(QApplication::translate("Udp1", "\345\217\221\351\200\201\346\241\206", nullptr));
        openButton->setText(QApplication::translate("Udp1", "\346\211\223\345\274\200", nullptr));
        closeButton->setText(QApplication::translate("Udp1", "\345\205\263\351\227\255", nullptr));
        pushButton_3->setText(QApplication::translate("Udp1", "\345\217\221\351\200\201", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Udp1: public Ui_Udp1 {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_UDP1_H
