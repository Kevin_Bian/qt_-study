#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QTime>
#include <QTimer>
#include <QDate>
#include <QDateTime>
#include <QDebug>

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();
    QTimer timer;
    QTime time,Time;
    QDate Date;
    QDateTime DateTime;

protected:
    void timerEvent(QTimerEvent *event);  //定时器事件

private slots:
    void on_startButton_clicked();
    void timeout_slot();
    void on_closeButton_clicked();

    void on_resetButton_clicked();

    void on_bitButton_clicked();

private:
    Ui::Widget *ui;
};
#endif // WIDGET_H
