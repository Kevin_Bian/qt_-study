#include "widget.h"
#include "ui_widget.h"
static int i;

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
//    Date = QDate::currentDate();  //获取系统当前日期
//    Time = QTime::currentTime();    //获取系统当前时间
//    ui->systemDate->setText(Date.toString("yyyy-MM-dd"));
//    ui->systemTime->setText(Time.toString("hh:mm:ss "));
    this->startTimer(500);


    connect(&timer,SIGNAL(timeout()),this,SLOT(timeout_slot()));
    time.setHMS(0,0,0,0);
    ui->showTime->setText("00:00:00.000");
    this->setWindowTitle("秒表");
}
Widget::~Widget()
{
    delete ui;
}
void Widget::timerEvent(QTimerEvent *event)  //每隔一秒来调用该函数
{
    DateTime = QDateTime::currentDateTime();   //获取系统的时间日期
    ui->systemDate->setText(DateTime.toString("yyyy-MM-dd hh:mm:ss dddd"));
}
void Widget::timeout_slot()
{
    time=time.addMSecs(100); //每次时间溢出后加100毫秒
    ui->showTime->setText(time.toString("hh:mm:ss.zzz"));

}

void Widget::on_startButton_clicked()
{
    timer.start(100);//每隔100毫秒溢出，执行timeout函数
}

void Widget::on_closeButton_clicked()
{
    timer.stop();
}

void Widget::on_resetButton_clicked()
{
    timer.stop();
    time.setHMS(0,0,0,0);
    ui->showTime->setText("00:00:00.000");
    ui->bitTimeEdit->clear();
    i=0;
}

void Widget::on_bitButton_clicked()
{
    QString temp ;
    i++;
    temp.sprintf("%d",i);
    ui->bitTimeEdit->appendPlainText(temp);
    ui->bitTimeEdit->appendPlainText(time.toString("hh:mm:ss.zzz"));
}
