#ifndef ENTERBUTTON_H
#define ENTERBUTTON_H

#include <QPushButton>


class enterButton : public QPushButton
{
    Q_OBJECT
public:
    explicit enterButton(QWidget *parent = nullptr);

protected:
    void enterEvent(QEvent *event);
    void leaveEvent(QEvent *event);

signals:

public slots:
};

#endif // ENTERBUTTON_H
