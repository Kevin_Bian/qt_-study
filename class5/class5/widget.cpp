#include "widget.h"
#include "ui_widget.h"
#include <QDebug>
#include <QPushButton>

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
}

Widget::~Widget()
{
    delete ui;
}

void Widget::enterEvent(QEvent *event)
{
    ui->pushButton->setText("不是");
}
void Widget::leaveEvent(QEvent *event)
{
    ui->pushButton->setText("是");
}

