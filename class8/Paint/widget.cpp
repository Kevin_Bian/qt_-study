#include "widget.h"
#include "ui_widget.h"
#include <QPen>
#include <QBrush>

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
    x=0;
    y=0;
}

Widget::~Widget()
{
    delete ui;
}
void Widget::paintEvent(QPaintEvent *event)
{
    QPainter p(this); //创建画家对象 this 指定绘图窗口为当前设备
//    p.drawPixmap(0,0,width(),height(),QPixmap("../qt2.png"));//0,0为添加图片的起始位置
//   p.drawPixmap(rect(),QPixmap("../qt2.png"));//rect（）直接回去窗体的矩形区域
    QPen pen; //创建画笔对象
    pen.setWidth(1);
    pen.setColor(Qt::green);
    pen.setColor(QColor(0,0,0));//通过RGB来设置颜色
    pen.setStyle(Qt::DashLine);

    QBrush brush; //创建画刷对象
    brush.setColor(Qt::black);
    brush.setStyle(Qt::Dense6Pattern);
    p.setPen(pen); //把设置好的笔交给画家
    p.setBrush(brush);
    p.drawLine(50,50,300,50);
    p.drawLine(50,50,50,300);
    p.drawRect(300,300,200,200); //x,y要画矩形的左上角起始坐标，后两个长和宽
    p.drawEllipse(QPoint(200,200),100,100); //圆心坐标，水平和垂直方向的半径长度
    p.drawPixmap(x,y,100,100,QPixmap("../face.png")); //画笑脸
}

void Widget::on_pushButton_clicked()
{
    x+=100;
    if(x>width()-100)
    {
        x=0;
        y+=100;
    }
    if(y>height()-100)
        y=0;
    update();//重绘窗口
}
